class CreateList < ActiveRecord::Migration[4.2]
  def change
    create_table :list do |t|
      t.string :task
      t.string :date
      t.string :description
      t.string :priority
    end
  end
