require_relative 'config/environment'

class App < Sinatra::Base

  enable :sessions

  @@tasks = []

  get '/' do
    erb :list_form
  end

  get '/list_form' do
    erb :list_form
  end

  post '/list' do
    @item = []
    @item.push(params[:task])
    @item.push(params[:due_date])
    @item.push(params[:description])
    @item.push(params[:priority])
    @@tasks << @item
    "#{@@tasks}"
  end

  get '/test' do
    "#{@@tasks}"
  end

end
